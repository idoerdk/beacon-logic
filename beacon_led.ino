#include <SoftPWM.h> // Install SoftPWM from Arduino IDE Library manager
#include <SoftPWM_timer.h> // Install SoftPWM from Arduino IDE Library manager

/*
  Simulated beacon light on 1 Arduino PIN usin PWM
    (c)iDoer ApS, Jens Arnt, jens@idoer.dk
    This code is released under the Apache  2.0 Licence
    Any feedback is appreciated 

    Will use the SoftPWM librray if it has been imported from the Arduino IDE

    Will only use hardware PWM on the pins designated ~on the Arduino if the SoftPWM library has not been included
*/

unsigned long timer1 = 0; // psudo timer used for updating the 
int light = 0; // Light level of the beacon_pin
unsigned int beacon_pin = 13; // The pin used for the beacon LED
unsigned long beacon_period = 1000; // full on/off period for beacon in ms



void setup()
{
  #ifdef SOFTPWM_H
   SoftPWMBegin();
  #endif

  #ifdef SOFTPWM_H
   // Create and set beacon_pin to 0 (off)
   SoftPWMSet(beacon_pin, 0);
   SoftPWMSetFadeTime(beacon_pin, 0, 0);
  #else
   pinMode(beacon_pin, OUTPUT);
  #endif
  timer1 = millis();

}

void loop()
{
  //This section goes to the beginning of the loop .. will not work properly if delay() is used anywhere
  if((millis()-timer1) >(beacon_period/255)) { // has we reached the time to update the beacon pin ?
    timer1=millis(); // Reset (kindoff) the timer 
    do_beacon(); // Do the beacon logic
  }
}

void do_beacon() {  
  if(++light>255) light=-254;
  #ifdef SOFTPWM_H
   SoftPWMSet(beacon_pin, abs(light));
  #else
   analogWrite(beacon_pin, abs(light));
  #endif
}
